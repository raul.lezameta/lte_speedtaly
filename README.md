# lte_speedtaly

This repository contains the code for a RESTful API to calculate mobile network speeds in Italy and display the results on a web map.

The used data comes from https://lteitaly.it/

> Note: This repository must be used in combination with https://gitlab.com/raul.lezameta/lte_speedtaly_db/, which provides the instructions to build the necessary PostGIS database.

# Getting Started:

1. Create a `.env` file and set the following variables for your db (those should match the variables of your database as set in https://gitlab.com/raul.lezameta/lte_speedtaly_db/)
```.env
DB_HOST="host:portnumber"
DB_USERNAME="username"
DB_PASSWORD="password"
```
2. Set up the database according to the instructions on https://gitlab.com/raul.lezameta/lte_speedtaly_db/
3. Install all the necessary Node.js packages using `npm install`
4. Run the Server by running `npm start` or by using the provided launch configurations if you are using VS Code