const createError = require("http-errors");
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");

const indexRouter = require("./routes/index");
const usersRouter = require("./routes/users");

const app = express();

// STATIC ASSETS:

app.use(express.static(path.join(__dirname, "public"))); // <- this line will us public directory as your static assets

app.use(
  "/stylesheets",
  express.static(path.join(__dirname, "node_modules/bootstrap/dist/css"))
); // <- This will use the contents of 'bootstrap/dist/css' which is placed in your node_modules folder as if it is in your '/stylesheets' directory.

app.use(
  "/js",
  express.static(path.join(__dirname, "node_modules/bootstrap/dist/js"))
); // <- This will use the contents of 'bootstrap/dist/js' which is placed in your node_modules folder as if it is in your '/js' directory.

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/users", usersRouter);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
