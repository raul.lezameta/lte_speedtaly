/* eslint-disable no-console */
/* eslint-disable camelcase */
/* global res_mno L myData res_lat res_lon res_zoom res_z */

document.getElementById("mno_select").value = res_mno;
// Create variable to hold map element, give initial settings to map

const map = L.map("map", { center: [res_lat, res_lon], zoom: res_zoom });

const Stamen_Terrain = L.tileLayer(
  "https://stamen-tiles-{s}.a.ssl.fastly.net/terrain/{z}/{x}/{y}{r}.{ext}",
  {
    attribution:
      'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    subdomains: "abcd",
    minZoom: 0,
    maxZoom: 15, // 18 was default!! 14 best (but very zoomed in view not possib  le)
    ext: "png",
    opacity: 0.6,
  }
);

const Stadia_OSMBright = L.tileLayer(
  "https://tiles.stadiamaps.com/tiles/osm_bright/{z}/{x}/{y}{r}.png",
  {
    maxZoom: 20,
    attribution:
      '&copy; <a href="https://stadiamaps.com/">Stadia Maps</a>, &copy; <a href="https://openmaptiles.org/">OpenMapTiles</a> &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
    opacity: 1,
  }
);

const HikeBike_HillShading = L.tileLayer(
  "https://tiles.wmflabs.org/hillshading/{z}/{x}/{y}.png",
  {
    maxZoom: 15,
    attribution:
      '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    opacity: 0.6,
  }
);

Stadia_OSMBright.addTo(map);
HikeBike_HillShading.addTo(map);

// Stamen_Terrain.addTo(map);

const colors = {
  WindTre: "#e48330",
  Iliad: "#44231c",
  TIM: "#0b3d86",
  Vodafone: "#cf181f",
};

if (res_mno !== "Select One...") {
  L.marker([res_lat, res_lon])
    .addTo(map)
    .bindTooltip(`height: ${Math.round(res_z)} m`)
    .openTooltip();
}

function get_lteItaly_link(mno, id) {
  const mno_id = {
    WindTre: 22288,
    TIM: 2221,
    Iliad: 22250,
    Vodafone: 22210,
  };

  return `https://lteitaly.it/internal/map.php#bts=${mno_id[mno]}.${id}`;
}

function upadateTable(properties) {
  const stats = properties.bands;
  const t = document.getElementById("stats");

  while (t.rows.length > 1) {
    t.deleteRow(1);
  }

  // add Data to Table
  stats.forEach((stat) => {
    const new_row = t.insertRow(1);

    new_row.appendChild(document.createElement("th"));

    const band_cell = new_row.children[0];
    // band_cell.outerHTML = "<th></th>";
    band_cell.innerHTML = stat.band;
    band_cell.scope = "row";

    const arfcn_cell = new_row.insertCell(1);
    arfcn_cell.innerHTML = stat.arfcn;

    const freq_cell = new_row.insertCell(2);
    freq_cell.innerHTML = stat.frequency_mhz;

    const thSpeed_cell = new_row.insertCell(3);
    thSpeed_cell.innerHTML = stat.max_speed;

    const range_cell = new_row.insertCell(4);
    range_cell.innerHTML = stat.max_range;

    const maxSpeed_cell = new_row.insertCell(5);
    maxSpeed_cell.innerHTML = stat.actual_speed;
  });

  document.getElementById("speed").innerHTML = Math.round(
    properties.total_speed
  );
  document.getElementById("site_name").innerHTML = properties.name;
  document.getElementById("dist").innerHTML =
    Math.round(properties.dist / 10) / 100;

  const lteItaly_link = get_lteItaly_link(properties.mno, properties.id);

  document.getElementById("link_lteItaly").innerHTML = lteItaly_link;
  document.getElementById("link_lteItaly").href = lteItaly_link;

  document.getElementById("visibility").innerHTML = properties.visible;
}
if (myData != null) {
  // Add JSON to map
  const ids = {};
  let id_count = 0;

  const bts_layer = L.geoJSON(myData, {
    pointToLayer(feature, latlng) {
      return L.circleMarker(latlng, {
        radius: 8,
        fillColor: colors[feature.properties.mno],
        color: "#000",
        weight: 1,
        opacity: 1,
        fillOpacity: feature.properties.visible ? 0.8 : 0.3,
        className: "bts-marker",
      });
    },
    onEachFeature(feature, layer) {
      ids[feature.properties.id] = id_count;
      id_count += 1;

      layer.bindTooltip(
        `<b> name: </b> ${feature.properties.name} <br>
       <b> id: </b> ${feature.properties.id} <br>
       <b> dist: </b> ${Math.round(feature.properties.dist / 10) / 100} km <br>
       <b> height: </b> ${Math.round(feature.geometry.coordinates[2])} m <br>
       <b> visible: </b> ${feature.properties.visible}`
      );
      layer.on("click", (e) => {
        console.log(feature.properties.id);
        console.log(`visible: ${feature.properties.visible}`);
        upadateTable(feature.properties);
      });
    },
  }).addTo(map);

  bts_layer.on("load", () => {
    console.log("layer laoded");
    document.getElementsByClassName("bts-marker").forEach((bts_m) => {
      bts_m.addEventListener("click", (event) => {
        console.log("clicked");
      });
    });
  });
} else {
  document.getElementById("band_stats").remove();
  document.getElementById("bts_stats").remove();
}

const lat_input = document.getElementById("lat_input");
const lon_input = document.getElementById("lon_input");
// Get Lat Lon from click
map.on("click", (e) => {
  const coord = e.latlng;
  const { lat } = coord;
  const lon = coord.lng;
  console.log(`You clicked the map at latitude: ${lat} and longitude: ${lon}`);
  lat_input.value = lat;
  lon_input.value = lon;
});

upadateTable(myData.features[0].properties);
