const express = require("express");
// require Express
const router = express.Router(); // setup usage of the Express router engine

/* PostgreSQL and PostGIS module and connection setup */
const { Client, Query } = require("pg");
require("dotenv").config();

// Setup connection
const username = process.env.DB_USERNAME; // sandbox username
const password = process.env.DB_PASSWORD; // read only privileges on our table
const host = process.env.DB_HOST;
const database = "gis"; // database name
const conString = `postgres://${username}:${password}@${host}/${database}`; // Your Database Connection

/* GET home page. */
router.get("/", (req, res, next) => {
  res.redirect("/map");
});

/* GET the map page */
router.get("/map", (req, res) => {
  res.render("map", {
    // render map.pug with following context:
    jsonData: null, // Pass data to the View
    set_lat: 42.7, //
    set_lon: 12.7, //
    mno: "Select One...",
    zoom: 6,
    z: 0,
  });
});

function process_db_result(data) {
  console.log("pass");
  data.features.forEach((feat) => {
    feat.properties.total_speed = 0;

    feat.properties.bands.forEach((band) => {
      feat.properties.total_speed += band.actual_speed;
    });
  });

  data.features.sort((a, b) => {
    const diff = // if diff is negative a is sorted before b
      b.properties.total_speed -
      a.properties.total_speed +
      !a.properties.visible * 10000 -
      !b.properties.visible * 10000;
    return diff;
  });
}

/* GET Postgres JSON data */
router.get("/data*", (req, res) => {
  const { mno } = req.query;
  const { lat } = req.query;
  const { lon } = req.query;
  if (!mno) {
    console.log("Mno has to be selected");
    res.redirect("/map");
  }
  if (
    mno.indexOf("--") > -1 ||
    mno.indexOf("'") > -1 ||
    mno.indexOf(";") > -1 ||
    mno.indexOf("/*") > -1 ||
    mno.indexOf("xp_") > -1
  ) {
    console.log("Bad request detected");
    res.redirect("/map");
  } else {
    console.log("Request passed");
    const filter_query = `SELECT bts_as_geojson('${mno}', ${lat}, ${lon});`;
    const client = new Client(conString);
    client.connect();
    const query = client.query(new Query(filter_query));
    query.on("row", (row, result) => {
      result.addRow(row);
    });
    query.on("end", (result) => {
      const data = result.rows[0].bts_as_geojson.bts_geojson;
      if (!data.features) {
        console.log("No bts found");
        res.redirect("/map");
      } else {
        process_db_result(data);

        res.send(data);
        res.end();
      }
    });
  }
});

/* GET the filtered page */
router.get("/filter*", (req, res) => {
  const { mno } = req.query;
  const { lat } = req.query;
  const { lon } = req.query;
  if (!mno) {
    console.log("Mno has to be selected");
    res.redirect("/map");
  } else if (
    mno.indexOf("--") > -1 ||
    mno.indexOf("'") > -1 ||
    mno.indexOf(";") > -1 ||
    mno.indexOf("/*") > -1 ||
    mno.indexOf("xp_") > -1
  ) {
    console.log("Bad request detected");
    res.redirect("/map");
  } else {
    console.log("Request passed");
    const filter_query = `SELECT bts_as_geojson('${mno}', ${lat}, ${lon});`;
    const client = new Client(conString);
    client.connect();
    const query = client.query(new Query(filter_query));
    query.on("row", (row, result) => {
      result.addRow(row);
    });
    query.on("end", (result) => {
      const data = result.rows[0].bts_as_geojson.bts_geojson;
      if (!data.features) {
        console.log("No bts found");
        res.redirect("/map");
      } else {
        process_db_result(data);

        const { z } = result.rows[0].bts_as_geojson;

        res.render("map", {
          // render map.pug with following context:
          jsonData: data,
          set_lat: lat,
          set_lon: lon,
          zoom: 12,
          z,
          mno,
        });
      }
    });
  }
});

module.exports = router;
